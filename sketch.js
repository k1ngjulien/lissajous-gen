let angle;
let cols;
let radius;
let colWidth;

let sideLength;
let figures;

let countSliderOutput;
let phaseSliderOutput;
let countSlider;
let phaseSlider;

let imageCanvas;
let cnv;

let phase;

class Figure {
  constructor() {
    this.points = []
    this.current = {x: 0, y:0}
  }

  setX(x) {
    this.current.x = x;
  }

  setY(y) {
    this.current.y = y;
  }

  addPoint() {
    this.points.push(this.current);
    this.current = [];
  }

  draw() {
    noFill();
    stroke(255);
    strokeWeight(2);
    beginShape();
    for(let pnt of this.points) {
      vertex(pnt.x, pnt.y);
    }
    endShape();
  }
}

function start() {
  init();
  loop();
}

function stop() {
  noLoop();
}

function sliderChange(e) {
  countSliderOutput.innerText = `Count: ${e.target.value}`;
}

function phaseSliderChange(e) {
  phaseSliderOutput.innerText = `Phase: ${e.target.value}/16 PI`;;

  
}

function setup() {
  countSliderOutput = document.getElementById('countSliderOutput');
  phaseSliderOutput = document.getElementById('phaseSliderOutput');
  countSlider = document.getElementById('countSlider');
  phaseSlider = document.getElementById('phaseSlider');
  
  countSlider.addEventListener('input', sliderChange);
  phaseSlider.addEventListener('input', phaseSliderChange);
  document.getElementById('startBtn').addEventListener('click', start)
  document.getElementById('stopBtn').addEventListener('click', stop)
  
  sideLength = 800;
  cols = 0;

  cnv = createCanvas(sideLength, sideLength);
  cnv.parent('canvasContainer');

  init();
  noLoop();
}

function init() {
  angle = 0.01;
  phase = Math.PI * (parseFloat(phaseSlider.value)/16);
  
  cols = parseInt(countSlider.value);

  colWidth = width / (cols + 1);  // +1 Because the first coll is emty
  radius = (colWidth / 2) - 10;

  pointPosTop = [];
  pointPosSide = [];

  figurePoints = [];

  for(let i = 0; i < cols; i++) {
    pointPosTop.push({x: null, y: null})
    pointPosSide.push({x: null, y: null})
  }

  figures = make2DArray(cols,cols);

  for (let i = 0; i < figures.length; i++) {
    for(let j = 0; j < figures[i].length; j++) {
      figures[i][j] = new Figure();
    }
  }
}


function draw() {
  background(0);
  stroke(255);
  noFill();

  // Circles on Top
  for(let i = 0; i < cols; i++) {
    let circleX = colWidth * i + colWidth * 1.5;
    let circleY = colWidth / 2;

    strokeWeight(2);
    ellipse(circleX, circleY, radius * 2);

    let pointX = circleX + radius * Math.cos(angle *(i+1) + phase);
    let pointY = circleY + radius * Math.sin(angle *(i+1) + phase);
    
    strokeWeight(10);
    point(pointX , pointY);
    
    strokeWeight(1);
    line(pointX, 0, pointX, height);

    for(let j = 0; j < cols; j++) {
      figures[i][j].setX(pointX)
    }
  }

  // Circles to the left
  for(let j = 0; j < cols; j++) {
    let circleX = colWidth / 2;
    let circleY = colWidth * j + colWidth * 1.5;

    strokeWeight(2);
    ellipse(circleX, circleY, radius * 2);

    let pointX = circleX + radius * Math.cos(angle *(j+1));
    let pointY = circleY + radius * Math.sin(angle *(j+1));
    
    strokeWeight(10);
    point(pointX , pointY);

    strokeWeight(1);
    line(0, pointY, width, pointY);

    for(let i = 0; i < cols; i++) {
      figures[i][j].setY(pointY)
    }
  }

  for (let i = 0; i < cols; i++) {
    for(let j = 0; j < cols; j++) {
      figures[i][j].addPoint();
      figures[i][j].draw();
    }
  }

  angle += 0.01;

  if(angle > Math.PI *2)
  {
    noLoop();
  }
}

function make2DArray(cols, rows) {
  let array = new Array(cols);

  for (let i = 0; i < array.length; i++) {
    array[i] = new Array(rows);
  }
  return array;
}
